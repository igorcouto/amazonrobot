/* globals resolve, offsets */
'use strict';

var df = (new Date()).getTimezoneOffset();

var notify = message => chrome.notifications.create({
  type: 'basic',
  iconUrl: 'data/icons/48.png',
  title: chrome.runtime.getManifest().name,
  message
});

var randoms = {};
chrome.tabs.onRemoved.addListener(tabId => delete randoms[tabId]);

var onCommitted = ({url, tabId, frameId}) => {
  if (url && url.startsWith('http')) {
    let location = localStorage.getItem('location');
    const standard = localStorage.getItem('standard');
    const daylight = localStorage.getItem('daylight');

    let offset = localStorage.getItem('offset') || 0;
    let msg = localStorage.getItem('isDST') === 'false' ? standard : daylight;

    if (localStorage.getItem('random') === 'true') {
      const ofs = Object.keys(offsets);
      if (frameId === 0 || randoms[tabId] === undefined) {
        location = ofs[Math.floor(Math.random() * ofs.length)];
        randoms[tabId] = location;
      }
      else {
        location = randoms[tabId];
      }
      const o = resolve.analyze(location);
      offset = o.offset;
      msg = offset !== o.offset ? o.storage.msg.daylight : o.storage.msg.standard;
    }
    chrome.tabs.executeScript(tabId, {
      runAt: 'document_start',
      frameId,
      matchAboutBlank: true,
      code: `document.documentElement.appendChild(Object.assign(document.createElement('script'), {
        textContent: 'Date.prefs = ["${location}", ${-1 * offset}, ${df}, "${msg}"];'
      })).remove();`
    }, () => chrome.runtime.lastError);
  }
};

var update = () => chrome.storage.local.get({
  enabled: true
}, ({enabled}) => {
  if (enabled) {
    chrome.webNavigation.onCommitted.addListener(onCommitted);
  }
  else {
    chrome.webNavigation.onCommitted.removeListener(onCommitted);
  }
  chrome.browserAction.setIcon({
    path: {
      '16': 'data/icons' + (enabled ? '' : '/disabled') + '/16.png',
      '32': 'data/icons' + (enabled ? '' : '/disabled') + '/32.png',
      '48': 'data/icons' + (enabled ? '' : '/disabled') + '/48.png',
      '64': 'data/icons' + (enabled ? '' : '/disabled') + '/64.png'
    }
  });
  // Updates the title extension too
  let offset = parseInt(localStorage.getItem('offset'));
  offset = offset / 60 * 100;
  let title_str = localStorage.getItem('location') +' (' + localStorage.getItem('offset') + '/' + offset + ')';
  set_extension_title(title_str);
});
chrome.storage.onChanged.addListener(prefs => {
  if (prefs.enabled) {
    update();
  }
});
update();

var set = (timezone = 'Etc/GMT') => {
  const {offset, storage} = resolve.analyze(timezone);
  localStorage.setItem('offset', offset);
  localStorage.setItem('isDST', offset !== storage.offset);
  localStorage.setItem('location', timezone);
  localStorage.setItem('daylight', storage.msg.daylight);
  localStorage.setItem('standard', storage.msg.standard);
};

chrome.browserAction.onClicked.addListener(() => {
    server(false);
});

var server = async(silent = true) => {
  try {
    const timezone = await resolve.remote();

    if (localStorage.getItem('location') !== timezone) {
        set(timezone);
        notify('Timezone is changed to ' + timezone + ' (' + localStorage.getItem('offset') + ')');
        // Updates the title extension too
        let offset = parseInt(localStorage.getItem('offset'));
        offset = offset / 60 * 100;
        let title_str = localStorage.getItem('location') +' (' + localStorage.getItem('offset') + '/' + offset + ')';
        set_extension_title(title_str);
    }
    else if (silent === false) {
      notify('Already in Timezone; ' + localStorage.getItem('offset') + ' (' + timezone + ')');
    }
  }
  catch(e) {
    if (silent === false) {
      notify(e.message);
    }
  }
};

// on installed
chrome.runtime.onInstalled.addListener(() => {
  server(false);
});
// update on startup?
{
  const callback = () => {
    if (localStorage.getItem('update') === 'true') {
      server(false);
    }
  };
  chrome.runtime.onInstalled.addListener(callback);
  chrome.runtime.onStartup.addListener(callback);
}
// context menu
{
  const callback = () => {
    chrome.contextMenus.create({
      title: 'Check my current timezone',
      id: 'check-timezone',
      contexts: ['browser_action']
    });
    chrome.contextMenus.create({
      title: 'Update timezone from IP',
      id: 'update-timezone',
      contexts: ['browser_action']
    });
    chrome.contextMenus.create({
      title: 'Delay in one hour',
      id: 'delay-offset',
      contexts: ['browser_action']
    });
    chrome.contextMenus.create({
      title: 'Advance in one hour',
      id: 'advance-offset',
      contexts: ['browser_action']
    });
  };
  chrome.runtime.onInstalled.addListener(callback);
  chrome.runtime.onStartup.addListener(callback);
}

chrome.contextMenus.onClicked.addListener(({menuItemId}) => {
  if (menuItemId === 'update-timezone') {
    server(false);
  }
  else if (menuItemId === 'check-timezone') {
    chrome.tabs.create({
      url: 'http://check2ip.com/'
    });
  }
  else if (menuItemId === 'delay-offset'){
    var current_offset = parseInt(localStorage.getItem('offset'));
    var delayed_offset = current_offset - 60;
    localStorage.setItem('offset', delayed_offset);
    notify('Offset delayed: ' + localStorage.getItem('offset'));
  }
  else if (menuItemId === 'advance-offset'){
    var current_offset = parseInt(localStorage.getItem('offset'));
    var advanced_offset = current_offset + 60;
    localStorage.setItem('offset', advanced_offset);
    notify('Offset advanded: ' + localStorage.getItem('offset'));
  }
});

/*
* Sets a new title to extension
*/
var set_extension_title = title_str => {
    chrome.browserAction.setTitle({title: title_str})
};

/*
* Fired when a tab is updated.
*/
chrome.tabs.onUpdated.addListener(function(tabId, props) {
    // If the IP changed at once, shows a alert.
    if (props.status == "complete") {
        server(true);
    }
});

/*
* Creates an alarm to execute every 1 minute.
*/ 
chrome.alarms.create("1min", {delayInMinutes: 1,
                              periodInMinutes: 1});

/*
* Adds the 1 minute alarm to check connection.
*/ 
chrome.alarms.onAlarm.addListener(function(alarm) {
    if (alarm.name === "1min") {
        server(true);
    }
});
