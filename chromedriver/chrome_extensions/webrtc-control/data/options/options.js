var load = function () {
  var input = document.querySelector("#inject");
  var select = document.querySelector("#method");
  var donation = document.querySelector("#donation");
  /*  */
  background.send("options:load");
  window.removeEventListener("load", load, false);
  /*  */
  donation.addEventListener("click", function (e) {background.send("options:donation")});
  select.addEventListener("change", function (e) {background.send("options:webrtc", {"webrtc": e.target.value})});
  input.addEventListener("change", function (e) {background.send("options:inject", {"inject": e.target.checked})});
};

background.receive("options:storage", function (e) {
  var input = document.querySelector("#inject");
  var select = document.querySelector("#method");
  /*  */
  if (e.webrtc) select.value = e.webrtc;
  if (e.inject) input.checked = e.inject;
});

window.addEventListener("load", load, false);
