var config = {};

config.webrtc = {"test": {"page": "https://webbrowsertools.com/ip-address/"}};

config.welcome = {
  set lastupdate (val) {app.storage.write("lastupdate", val)},
  get lastupdate () {return app.storage.read("lastupdate") !== undefined ? app.storage.read("lastupdate") : 0}
};

config.addon = {
  set state (val) {app.storage.write("state", val)},
  set inject (val) {app.storage.write("inject", val)},
  set webrtc (val) {app.storage.write("webrtc", val)},
  get inject () {return app.storage.read("inject") !== undefined ? app.storage.read("inject") : true},
  get state () {return app.storage.read("state") !== undefined ? app.storage.read("state") : "enabled"},
  get webrtc () {return app.storage.read("webrtc") !== undefined ? app.storage.read("webrtc") : "disable_non_proxied_udp"}
};
