'use strict';

/*
* Shows an alert box.
*/
var show_alert = message => {alert(message);}

/*
* Function to retrieve information of current connection.
*/
var get_connection = () => fetch('https://ipapi.co/json')
    .then(r => r.json())
    .then(j => {
        const connection = {}
        if (j && j.ip) {connection.ip = j.ip}
        if (j && j.country_name) {connection.country = j.country_name}
        if (j && j.region) {connection.state = j.region}
        if (j && j.city) {connection.city = j.city}
        if (j && j.timezone) {connection.timezone = j.timezone}
        if (j && j.utc_offset) {connection.utc_offset = j.utc_offset}
        if (Object.keys(connection).length > 0){return connection;}
        else {
            throw Error('It is not possible to retrieve connection status!');
        }
    });

/*
* Sets a new title to extension
*/
var set_extension_title = title_str => {
    chrome.browserAction.setTitle({title: title_str})
};

/*
* Saves the connection using chrome.storage API.
* @param    {Object}    connection  The connection information
*/
var save = (conn) => {
    // If there was an previous IP, move it to a new variable.
    if (localStorage.getItem('IP')){
        localStorage.setItem('PREVIOUS_IP', localStorage.getItem('IP'));
        localStorage.setItem('PREVIOUS_COUNTRY', localStorage.getItem('COUNTRY'));
        localStorage.setItem('PREVIOUS_STATE', localStorage.getItem('STATE'));
        localStorage.setItem('PREVIOUS_CITY', localStorage.getItem('CITY'));
        localStorage.setItem('PREVIOUS_TZ', localStorage.getItem('TZ'));
        localStorage.setItem('PREVIOUS_OFF', localStorage.getItem('OFF'));
    }
    // Update IP status
    if (conn.ip) {localStorage.setItem('IP', conn.ip);}
    if (conn.country) {localStorage.setItem('COUNTRY', conn.country);}
    if (conn.state) {localStorage.setItem('STATE', conn.state);}
    if (conn.city) {localStorage.setItem('CITY', conn.city);}
    if (conn.timezone) {localStorage.setItem('TZ', conn.timezone);}
    if (conn.utc_offset) {localStorage.setItem('OFF', conn.utc_offset);}

    // Updates the title extension too
    let title_str = conn.ip + '\n'
                    + conn.city + ', ' + conn.state + '. ' + conn.country + '\n'
                    + conn.timezone + ' (' + conn.utc_offset + ')';
    set_extension_title(title_str);
}

/*
* Notifies the user using chrome.notifications API.
* @param    {string}    message The message to be displayed.
*/
var notify = message => chrome.notifications.create({
    type: 'basic',
    iconUrl: 'icons/64.png',
    title: chrome.runtime.getManifest().name,
    message
});

/* 
* Checks if the connection has changed from the last check.
* If changed, saves the new connection and notifies the user. 
*/
var check_connection = async(silent=true) => {
    try{
        // Gets connection data
        let conn = await get_connection();
        // Compares with current IP
        if (conn.ip != localStorage.getItem('IP')){
            // Persists the current IP on chrome.storage API.
            save(conn);
            alert('WARNING! IP has changed:\n'
                + ' - ' + localStorage.getItem('PREVIOUS_IP') + ' -> ' + localStorage.getItem('IP') + '\n'
                + ' - ' + localStorage.getItem('PREVIOUS_CITY') + ' -> ' + localStorage.getItem('CITY') + '\n'
                + ' - ' + localStorage.getItem('PREVIOUS_STATE') + ' -> ' + localStorage.getItem('STATE') + '\n'
                + ' - ' + localStorage.getItem('PREVIOUS_COUNTRY') + ' -> ' + localStorage.getItem('COUNTRY') + '\n'
                + ' - ' + localStorage.getItem('PREVIOUS_TZ') + ' (' + localStorage.getItem('PREVIOUS_OFF') + ')'
                + ' -> '
                + localStorage.getItem('TZ') + ' (' + localStorage.getItem('OFF') + ')'
            );
        } else {
            if (!silent){
                notify('No changes were detected.');
            }
        }
    } catch (error) {
        notify(error.message);
    }
}

/*
* Fired when the extension is installed.
*/
chrome.runtime.onInstalled.addListener(async() => {
    // Gets connection data
    let conn = await get_connection();
    // Persists on chrome.storage API.
    save(conn);
});

/*
* Fired when a tab is updated.
*/
chrome.tabs.onUpdated.addListener(function(tabId, props) {
    // If the IP changed at once, shows a alert.
    if (props.status == "complete") {
        check_connection(true);
    }
});

/*
* Fired when the user clicks on the extension icon
*/ 
chrome.browserAction.onClicked.addListener(async() => {
    check_connection(false);
});

/*
* Creates an alarm to execute every 1 minute.
*/ 
chrome.alarms.create("1min", {
    delayInMinutes: 1,
    periodInMinutes: 1
});

/*
* Adds the 1 minute alarm to check connection.
*/ 
chrome.alarms.onAlarm.addListener(function(alarm) {
    if (alarm.name === "1min") {
        check_connection(true);
    }
});
