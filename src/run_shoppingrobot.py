import argparse
from random import choice
from common import support
from robots.shoppingrobot import ShoppingRobot
from resources.googlespreadsheet import GoogleSpreadSheet

import csv, os


def read_csv(file):
    file_path = os.path.abspath(file)
    data = []
    with open(file_path) as f:
        reader = csv.DictReader(f)
        for r in reader:
            data.append(dict(r))

    return data

def write_csv(file, data):
    if isinstance(data, list):
        columns = list(data[0].keys())
    else:
        columns = list(data.keys())

    file_path = os.path.abspath(file)
    header = False
    if not os.path.exists(file_path):
        header = True

    with open(file_path, 'a', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=columns)
        if header:
            writer.writeheader()
        # Write the data    
        if isinstance(data, list):
            for d in data:
                writer.writerow(d)
        else:
            writer.writerow(data)

def get_all_csv(file, columns_as_list, columns_as_bool):
    data = read_csv(file)
    split_strip = lambda string: [s.strip() for s in string.split(',')]

    for c in data:
        c.update((k, True) for k, v in c.items() if k in columns_as_bool and v)
        c.update((k, False) for k, v in c.items() if k in columns_as_bool and not v)
        c.update((k, split_strip(v)) for k, v in c.items() if k in columns_as_list and v)
    
    return data

def get_selected_rows(file, columns_as_list, columns_as_bool):
    data = read_csv(file)
    data = [c for c in data if c['select']]
    split_strip = lambda string: [s.strip() for s in string.split(',')]

    for c in data:
        c.update((k, True) for k, v in c.items() if k in columns_as_bool and v)
        c.update((k, False) for k, v in c.items() if k in columns_as_bool and not v)
        c.update((k, split_strip(v)) for k, v in c.items() if k in columns_as_list and v)
    
    return data

def run_a_thread(option):
    cfg = support.load_config('settings/shoppingrobot.ini')
    if option.offline:
        print('Loading customers data from %s.' % cfg['customers'])
        customers = get_selected_rows(cfg['customers'],
                                      columns_as_list=['specific_asins'],
                                      columns_as_bool=['select', 'empty_cart', 'add_to_wish_list', 'sponsored',
                                                       'non_sponsored', 'purchase_the_first'])

    else:
        gspreadsheet = GoogleSpreadSheet(credentials_file='credentials/gcp_credentials.json')
        customers = gspreadsheet.get_selected_rows(title='customers',
                                                   columns_as_list=['specific_asins'],
                                                   columns_as_bool=['select', 'empty_cart', 'add_to_wish_list', 'sponsored',
                                                                    'non_sponsored', 'purchase_the_first'])
    customer = choice(customers)

    if customer['specific_asins']:
        if option.offline:
            print('Loading customers data from %s.' % cfg['products'])
            all_products = get_all_csv(cfg['products'],
                                       columns_as_list=['asins', 'keywords', 'weights'],
                                       columns_as_bool=['select'])

        else:
            all_products = gspreadsheet.get_all_spreadsheet('products',
                                                            columns_as_list=['asins', 'keywords', 'weights'],
                                                            columns_as_bool=['select'])
        products = []
        for asin in customer['specific_asins']:
            for product in all_products:
                if asin in product['asins']:
                    product['asins'] = [asin]
                    products.append(product)
    else:
        if option.offline:
            print('Loading products data from %s.' % cfg['products'])
            products = get_selected_rows(cfg['products'],
                                         columns_as_list=['asins', 'keywords', 'weights'],
                                         columns_as_bool=['select'])
        else:
            products = gspreadsheet.get_selected_rows(title='products',
                                                    columns_as_list=['asins', 'keywords', 'weights'],
                                                    columns_as_bool=['select'])

    product = choice(products)
    keyword = support.weighted_choice(sequence=product['keywords'], weights=product['weights'])
    purchase_the_first = customer['purchase_the_first']
    try:

        while True:
            robot = ShoppingRobot(use_login=cfg['use_login'],
                                  wait_time=cfg['wait_time'],
                                  customer_email=customer['Email'],
                                  customer_pwd=customer['Password'],
                                  local_timezone=cfg['local_timezone'])
            robot.create_browser(chromedriver_path='chromedriver/chromedriver.exe',
                                proxy_extension_path='chromedriver/ProxyAutoAuth2-0.crx',
                                use_proxy=cfg['use_proxy'],
                                user_agent=customer['user_agent'],
                                proxy_service=cfg['proxy_service'],
                                proxy_user=cfg['proxy_user'],
                                proxy_pwd=cfg['proxy_pwd'])

            if not robot.go_to_initial_page():
                robot.quit()
                continue
            
            # Login process
            robot.sign_in()
            robot.fill_login_form()
            
            if robot.verification_needed():
                code = robot.wait_for_verification_code()
                robot.enter_the_code(code)
            
            robot.not_now()

            if customer['empty_cart']:
                robot.empty_cart()
            
            robot.type_on_search_bar(keyword=keyword)

            products_on_listing = robot.products_on_listing(asins=product['asins'],
                                                            keyword=keyword,
                                                            page_limit=cfg['page_limit'],
                                                            stop_at_the_first=purchase_the_first)
            if option.offline and products_on_listing:
                print('Saving tracking data in %s.' % cfg['products_tracking'])
                write_csv(cfg['products_tracking'], products_on_listing)
            else:
                for p in products_on_listing:
                    gspreadsheet.append_row_by_dict(title='products_tracking', _dict=p)

            values = {'sponsored': customer['sponsored'],
                        'non_sponsored': customer['non_sponsored']}

            products_on_listing = support.filter_list(products_on_listing, 'status_on_listing', values)
            
            if not products_on_listing:
                break
            
            if not purchase_the_first:
                break
            
            product_link = products_on_listing[0]['link']
            robot.get(product_link)
            robot.close_popup()

            if customer['add_to_wish_list']:
                robot.add_to_wish_list()

            # Product page actions
            robot.see_images()
            robot.see_and_choose_a_color()
            robot.scroll_down()
            robot.scroll_up()
            robot.see_popup_reviews()
            currentHeight, finalHeight = robot.see_reviews()
            robot.scroll_down(currentHeight, finalHeight)
            robot.scroll_up()
            robot.choose_quantity()

            # Add to cart and checkout process!
            if robot.add_to_cart():
                robot.add_to_your_order_modal()
                robot.add_to_your_order_side_pane()

            robot.click_on_proceed_to_checkout()
            robot.find_shipping_address_form()
            row = robot.get_output()

            if option.offline:
                print('Saving robot output data in %s.' % cfg['robot_output'])
                write_csv(cfg['robot_output'], [dict(zip(robot.HEADER, row))])
            else:
                gspreadsheet.append_row(title='shoppingrobot', header=robot.HEADER, row=row)

            break

    except Exception:
        print('!!!!!!!!!!! An unexpected error occured. !!!!!!!!!!')
        robot.quit()

    robot.quit()

def main():
    parser = argparse.ArgumentParser(description='ShoppingRobot')
    parser.add_argument('--offline', dest='offline', action='store_true',
                        help='If passed, the robot will load data from DATA folder.')
    args = parser.parse_args()

    i = 1
    while i<400:
        print('\n===== Interaction %s' % i)
        run_a_thread(args)
        i+=1

if __name__ == "__main__":
    main()
