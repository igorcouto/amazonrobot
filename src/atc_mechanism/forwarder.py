import sys
from pymongo import MongoClient
from pymongo.errors import InvalidURI

from random import choice
from selenium.webdriver.support.ui import WebDriverWait


def create_mongo_connection(cluster, username, password):
    """Creates a connection to MongoDB on Atlas service.

    Parameters
    ----------
    cluster : string
        The cluster name to connect
    username : string
        The username to connect on MongoDB Atlas
    password : string
        The password to connect on MongoDB Atlas
    Returns
    -------
    client : MongoClient
        The object with connection itself
    """
    host = '%s.mongodb.net' % cluster
    conn_string = 'mongodb+srv://%s:%s@%s/test?retryWrites=true&w=majority' % (username,
                                                                               password,
                                                                               host)
    # Try retrieve a connection from MongoDB Atlas
    try:
        client = MongoClient(conn_string)
    except InvalidURI:
        sys.exit('\nAn error occurred to access MongoDB Atlas. Please check if the' +
                 ' parameters are configured on configuration file.')
    
    return client

def get_link(options):
    """Gets a link from atc_mechanism collection from MongoDB Atlas.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    Returns
    -------
    link : dict
        The retrieved link.
    """
    if options.atc_mechanism:
        filter_types = []
        for _type in options.atc_mechanism:
            filter_types.append({'type': _type})
        query = { '$or': filter_types}
    else:
        query = {}

    # Retrieve the data based on query and choice one.
    client = create_mongo_connection('resources-g6bq8', 'shopping', 'shopping')
    cursor = client.atc_mechanism.links.find(query)
    links = [c for c in cursor]
    link = choice(links)

    return link

def forward_by_external_link(driver, options):
    """Forwards the browser instance to Amazon by a predefined link.

    Parameters
    ----------
    driver : webdriver.Chrome
        The current browser instance.
    options : a namespace object
        The parameters passed by command line.
    """
    print('\nATC MECHANISM: ')
    link = get_link(options)
    print('Forwarding the browser via %s: %s' % (link['type'], link['url']))
    if not link:
        return None
    else:
        if 'youtube' in link['type']:
            driver.get(link['url'])
            driver.find_element_by_id('invalid-token-redirect-goto-site-button').click()
        if 'pinterest' in link['type']:
            driver.get(link['url'])
            wait = WebDriverWait(driver, 10)
            pin_header_elmt = wait.until(
                lambda d: d.find_element_by_xpath('//section[@data-test-id="pinHeader"]')
            )
            pin_header_elmt.find_element_by_xpath('//div[text()="Visit"]').click()
            # Closing and changing to new tab
            driver.close()
            new_tab = driver.window_handles[-1]
            driver.switch_to.window(new_tab)
        if link['type'] in ['email', 'amazon_affiliate']:
            driver.get(link['url'])
