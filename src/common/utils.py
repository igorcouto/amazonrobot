import os
import csv
import logging
import configparser
from time import sleep
from random import randint


def read_config(file):
    """Reads .ini file from absolute path
    
    Parameters
    ----------
    file : str
        a .ini file

    Returns
    -------
    config : configparser.ConfigParser
        A ConfigParser object
    """
    file_path = os.path.abspath(file)
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(file_path)
    return config

def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    return logger

def random_sleep(min=3, max=7):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.
    
    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
        
    """
    if max <= min:
        max = min + 3
    sleep(randint(min, max))

def read_csv(file, delimiter=',', quotechar='"'):
    data = []
    
    with open(file) as f:
        reader = csv.DictReader(f, delimiter=delimiter, quotechar=quotechar)
        for r in reader:
            data.append(dict(r))
    
    return data

if __name__ == "__main__":
    pass