from random import randint


def set_proxy(options, chrome_options, configurations):
    """Sets the proxy service based on command line options.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    chrome_options: webdriver.ChromeOptions
        An object with options to ChromeDriver.
    
    Returns
    -------
    (chrome_options, proxy_service) : A tuple with a webdriver.ChromeOptions and
    a dictionary.
        The object with options to ChromeDriver with a proxy configuration added and
        a dictionary with proxy services information.
    """
    proxy_service = {}
    # Microleaves proxy service. No authentication needed.
    if options.proxy_service == 'microleaves':
        if options.proxy_server_ip:
            server = options.proxy_server_ip
        else:
            server = '62.210.169.169'

        if options.proxy_server_port:
            port = options.proxy_server_port
        else:
            port = 3266 + randint(0, 7)

        proxy_host = '%s:%s' % (server, port)
        proxy_service['proxy_host'] = proxy_host
        # Adding on ChromeOption object
        chrome_options.add_argument('--proxy-server=%s' % proxy_host)
        
    # NetNut proxy service. Authentication needed.
    if options.proxy_service == 'netnut':
        proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
        proxy_service['proxy_host'] = proxy_host
        proxy_service['proxy_user'] = options.proxy_user
        proxy_service['proxy_pwd'] = options.proxy_pwd
        # Adding on ChromeOption object
        chrome_options.add_argument('--proxy-server=%s' % proxy_host)
        chrome_options.add_extension(configurations['EXTENSIONS']['proxy_autoauth'])

    return chrome_options, proxy_service

def proxy_authentication(driver, options):
    """Authenticates on proxy service if needed.

    Parameters
    ----------
    driver : webdriver.Chrome
        The webdriver object.
    options : a namespace object
        The parameters passed by command line.
    """
    if options.proxy_service == 'netnut':
        driver.find_element_by_id('login').send_keys(options.proxy_user)
        driver.find_element_by_id('password').send_keys(options.proxy_pwd)
        driver.find_element_by_id('save').click()
    