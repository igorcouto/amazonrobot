import gspread
from resources.utility import get_logger, read_csv
from googleapiclient import discovery
from gspread.exceptions import SpreadsheetNotFound
from oauth2client.service_account import ServiceAccountCredentials


class GoogleSpreadSheet():
    def __init__(self, credentials_file):
        self.logger = get_logger('GoogleSpreadSheet')
        scope = ['https://spreadsheets.google.com/feeds',
                 'https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_file,
                                                                       scope)
        self.logger.info('Credentials: (%s).' % credentials_file)
        
        # Google SpreadSheet object.
        self.SERVICE = discovery.build('sheets',
                                       'v4',
                                       credentials=credentials,
                                       cache_discovery=False)
        self.CLIENT = gspread.authorize(credentials)

    def load_csv_on_spreadsheet(self, spreadsheet, file, range='A1', valueInputOption='RAW'):
        # Import data from CSV file.
        csv_data = read_csv(file=file)

        if not csv_data:
            return None
        
        # Continues if csv_data is not empty.
        data = []
        header = list(csv_data[0].keys())
        data.append(header)
        for csv_d in csv_data:
            data.append([d for d in csv_d.values()])

        body = {'values': data}

        result = self.SERVICE.spreadsheets().values().update(spreadsheetId=spreadsheet['id'],
                                                             range=range,
                                                             valueInputOption=valueInputOption,
                                                             body=body).execute()

        self.logger.info('%s cells were updated on "%s" spreadsheet.' % (result.get('updatedCells'),
                                                                         spreadsheet['name']))

    def load_list_of_dicts_on_spreadsheet(self, spreadsheet, list_of_dicts, range='A1', valueInputOption='RAW'):
        if not list_of_dicts:
            return None
        
        data = []
        header = list(list_of_dicts[0].keys())
        data.append(header)
        for _dict in list_of_dicts:
            data.append([d for d in _dict.values()])

        body = {'values': data}

        result = self.SERVICE.spreadsheets().values().update(spreadsheetId=spreadsheet['id'],
                                                             range=range,
                                                             valueInputOption=valueInputOption,
                                                             body=body).execute()

        self.logger.info('%s cells were updated on "%s" spreadsheet.' % (result.get('updatedCells'),
                                                                         spreadsheet['name']))

    def get_all_spreadsheet(self, title, columns_as_list=[], columns_as_bool=[], delimiter=','):
        retrieved_data = self.CLIENT.open(title=title).sheet1.get_all_records()
        all_data = []
        for data in retrieved_data:
            if columns_as_list:
                for col in columns_as_list:
                    if data[col]:
                        data[col] = [d.strip() for d in data[col].split(delimiter)]
            if columns_as_bool:
                for col in columns_as_bool:
                    if data[col]:
                        data[col] = True
                    else:
                        data[col] = False
            all_data.append(data)
        
        return all_data
    
    def get_selected_rows(self, title, columns_as_list=[], columns_as_bool=[], delimiter=','):
        retrieved_data = self.get_all_spreadsheet(title=title,
                                                  columns_as_list=columns_as_list,
                                                  columns_as_bool=columns_as_bool,
                                                  delimiter=delimiter)
        selected_data = []
        for data in retrieved_data:
            # If row is select, append.
            if data['select']:
                selected_data.append(data)

        return selected_data
    
    def append_row(self, title, header, row):
        sheet = self.CLIENT.open(title=title).sheet1
        if not sheet.row_values(1):
            sheet.append_row(header)
        sheet.append_row(row)

    def append_row_by_dict(self, title, _dict):
        sheet = self.CLIENT.open(title=title).sheet1
        header = sheet.row_values(1)
        row = []
        for h in header:
            row.append(str(_dict.get(h)))
        
        sheet.append_row(row)