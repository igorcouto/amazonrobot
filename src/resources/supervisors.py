import pprint
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class element_text_is_not(object):
    """A custom Expected Conditions to wait a change in HTML element text.

    Parameters
    ----------
    object : tuple
        A tuple with the HTML element and the text to be compared with.
    Returns
    -------
    element : HTML element
        The HTML element to wait
    """
    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        element = driver.find_element(*self.locator)
        if self.text != element.text:
            return element
        else:
            return False

def is_a_buyer_environment(driver, attempts=2):
    """Opens the check2ip.com page and checks if the browser instance (driver)
    is a buyer environment. 
    
    Parameters
    ----------
    driver : webdriver.Chrome
        The current browser instance.
    attempts : int
        The number of attemts that will be make. Sometimes, the extensions
        doesn't have time to change parameters in browser, so it is necessary
        reload the page. By default, the check2ip.com page will be loaded 2
        times.
    Returns
    -------
    buyer_environment : bool
        If True, it is a buyer environment.
    """
    i = 1
    while (i <= attempts):
        # Open the verification page.
        driver.get('http://check2ip.com')
        # An WebDriverWait object to wati for HTML changes.
        wait = WebDriverWait(driver, 60)
        try:
            wait.until(element_text_is_not((By.ID, '1157'),
                                            'checking...'))
            wait.until(element_text_is_not((By.ID, 'localip'),
                                            'updating.....'))
            wait.until(element_text_is_not((By.ID, '55577'),
                                            'Deep scan, updating.....'))
        except TimeoutException:
            print('It is not possible find some HTML elements on page. Reloading the page.')
            continue

        # Checks if some parameter is not correct
        bl_xpath = '//*[@id="1151"]//tr'
        split = lambda x: {'site': x.split(' ', 1)[0], 'status': x.split(' ', 1)[1]}
        bl_checklist = [split(e.text) for e in driver.find_elements_by_xpath(bl_xpath)]
        is_blacklisted_filter = lambda x: not('IP IS NOT' in x['status'])
        blacklisted_list = list(filter(is_blacklisted_filter, bl_checklist))
        if blacklisted_list:
            print('Sorry, the current IP is blacklisted.')
            i+=1
            break

        sys_tz_status = driver.find_element_by_xpath('//*[@id="1157"]/font').text
        if not sys_tz_status == 'OK':
            print('System TimeZone retuns a %s state.' % sys_tz_status)
            i+=1
            continue

        int_ip_status = driver.find_element_by_xpath('//*[@id="localip"]/code').text
        if not int_ip_status == 'N/A':
            print('Internal IP returns a %s state.' % int_ip_status)
            i+=1
            continue
        
        print('Your browser/connection is a buyer environment.')
        
        return True
    
    print('Your browser/connection it is not a buyer connection. Sorry!')

    return False

def load_cookies(driver, cookies, page='https://amazon.com'):
    """Loads the cookies on browser instance.

    Parameters
    ----------
    cookies : dict
        The cookies to load on browser instance.
    """
    driver.get(page)
    # Loading the cookies on browser instance.
    for c in cookies:
        if 'expiry' in c:
            del c['expiry']
        driver.add_cookie(c)
    # Reload page
    driver.get(page)