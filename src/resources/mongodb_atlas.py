from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError, CollectionInvalid


class MongoDB_Atlas():
    def __init__(self, cluster, username, password):
        conn_string = 'mongodb+srv://%s:%s@%s.mongodb.net/test?retryWrites=true&w=majority' % (username,
                                                                                               password,
                                                                                               cluster)
        # An objeto to access databases on cluster.
        self.client = MongoClient(conn_string)
    
    def get_database(self, db_name):
        return self.client.get_database(db_name)

    def create_database(self, db_name):
        return self.client.db_name
    
    def create_collection(self, db_name, col_name, index=None):
        db = self.client[db_name]
        try:
            collection = db.create_collection(col_name)
            if index:
                collection.create_index(index, unique=True)
        
        except CollectionInvalid:
            collection = db[col_name]
            print('%s collection already exists.' % col_name)

        return collection
    
    def insert_data_from_list(self, collection, data):
        for d in data:
            try:
                collection.insert_one(d).inserted_id
            except DuplicateKeyError:
                print('DuplicateKey: %s. Not inserted.' % d['_id'])
    
    def insert_one(self, collection, data):
        try:
            collection.insert_one(data).inserted_id
            return True
        except DuplicateKeyError:
            print('DuplicateKey: %s' % data['_id'])
   
    def update_one(self, collection, query, new_values):
        collection.update_one(query, new_values, upsert=True)

if __name__ == "__main__":
    pass