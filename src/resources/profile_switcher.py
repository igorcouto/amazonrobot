import sys
import tzlocal
from random import choice
from selenium import webdriver
from pymongo.errors import InvalidURI
from resources.mongodb_atlas import MongoDB_Atlas
from datetime import datetime as dt, timedelta as td


def _get_profile_from_cloud(db, email=None, last_access_days=7):
    """Gets a profile from MongoDB Atlas cluster. It considers that the database
    has a the following collections personal_info, cookies and settings.
    If an email was not supplied, a profile will be chosen randomly.

    Parameters
    ----------
    db : string
        The database name.
    email : string
        The email address of the Amazon Account

    Returns
    -------
    (personal_info, cookies, settings) : a tuple of dictionaries.
        Each dictionary has an aspect of profile.
    """
    # Defines a timezone aware datetime object based on last_access_days option
    timezone = tzlocal.get_localzone()
    now = dt.now(tz=timezone)
    days_ago = td(days=last_access_days)
    last_access_date = now - days_ago

    personal_info = {}
    if email:
        personal_info = db.personal_info.find_one({'email': email,
                                                   'last_access': {'$lt': last_access_date}})
        cookies = db.cookies.find_one({'email': email})
        settings = db.settings.find_one({'email': email})
    else:
        cursor = db.personal_info.aggregate([{'$match': {'last_access': {'$lt': last_access_date}}}])
        results = []
        for c in cursor:
            results.append(c)

        personal_info = choice(results)
        if personal_info:
            cookies = db.cookies.find_one({'email': personal_info['email']})
            # Excludes a bad cookie
            if cookies:
                for c in cookies['cookies']:
                    if 'expiry' in c:
                        del c['expiry']

            settings = db.settings.find_one({'email': personal_info['email']})
    
    # If the email and last_access_days returns no profile. Exit the programn
    if not personal_info:
        sys.exit('\nIt was not possible to find a profile that attends the USERNAME and LAST_ACCESS_DAYS'+
                 ' parameters.')

    # Assembles a profile
    profile = (personal_info, cookies, settings)

    return profile

def _update_last_access(db, email):
    """Updates last access attribute on personal_info collection.

    Parameters
    ----------
    db : string
        The database name.
    email : string
        The email address of the Amazon Account
    """
    # Datetime object was timezone aware
    timezone = tzlocal.get_localzone()
    now = dt.now(tz=timezone)
    
    # Updating last_access of profile
    query = {'email': email}
    new_values = {'$set': {'last_access': now}}
    db.personal_info.update_one(query, new_values, upsert=True)

def get_profile(options, configurations):
    """Gets a profile (personal_info, cookies and settings) based on options passed by
    command line and configurations file.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    configurations : dict
        A dict loaded by .INI file.
    
    Returns
    -------
    (personal_info, cookies, settings, opt) : a tuple of dictionaries.
        Each dictionary has an aspect of profile and options of a browser.
    """
    # Try retrieve a connection from MongoDB Atlas
    try:
        mongo = MongoDB_Atlas(cluster=configurations['MONGODB']['mdb_cluster'],
                              username=configurations['MONGODB']['mdb_username'],
                              password=configurations['MONGODB']['mdb_password'])
        customers_db = mongo.get_database(configurations['MONGODB']['mdb_database'])
    except InvalidURI:
        sys.exit('\nAn error occurred to access MongoDB Atlas. Please check if the parameters are configured '+
                 'on %s file.' % options.config_file)

    # If a username (email) was provided, it checks if there is a profile information on the cloud
    if options.username:
        mdb_personal_info, mdb_cookies, mdb_settings = _get_profile_from_cloud(customers_db,
                                                                               email=options.username,
                                                                               last_access_days=options.last_access_days)
        if not mdb_personal_info and not options.password:
            sys.exit('\nA password was not found for this Amazon account on the cloud. '+
                     'Please provide a password using the username (-u) flag.')

    # If not, a random profile will be chosen
    else:
        mdb_personal_info, mdb_cookies, mdb_settings = _get_profile_from_cloud(customers_db,
                                                                               last_access_days=options.last_access_days)

    # If the username supplied doesn't have personal_info, create an object with it
    if not mdb_personal_info:
        mdb_personal_info = {'email': options.username,
                             'password': options.password}

    # Preparing the browser settings to be used on ChromeDriver.
    opt = webdriver.ChromeOptions()
    if mdb_settings:
        opt.add_argument('--lang=%s' % mdb_settings.get('languages'))
        opt.add_argument('user-agent="%s"' % mdb_settings.get('user_agent'))
        opt.add_experimental_option('prefs', {'intl.accept_languages': mdb_settings.get('languages')})
        opt.add_argument('window-size=%s,%s' % (mdb_settings.get('w_resolution'),
                                                mdb_settings.get('h_resolution')))

    # If the profile doesn't have cookies, create one.
    if not mdb_cookies:
        mdb_cookies = {'email': mdb_personal_info['email'],
                       'cookies': None}

    _update_last_access(customers_db, mdb_personal_info['email'])

    # Closing connection
    mongo.client.close()

    return mdb_personal_info, mdb_cookies, mdb_settings, opt

def update_cookies(options, configurations, driver, email):
    # Try retrieve a connection from MongoDB Atlas
    try:
        mongo = MongoDB_Atlas(cluster=configurations['MONGODB']['mdb_cluster'],
                              username=configurations['MONGODB']['mdb_username'],
                              password=configurations['MONGODB']['mdb_password'])
        customers_db = mongo.get_database(configurations['MONGODB']['mdb_database'])
    except InvalidURI:
        sys.exit('\nAn error occurred to access MongoDB Atlas. Please check if the parameters are configured '+
                 'on %s file.' % options.config_file)
    
    profiles = mongo.get_database('profiles')
    new_cookies = driver.get_cookies()
    for c in new_cookies:
        if 'expiry' in c:
            del c['expiry']
        
    query = {'email': email}
    new_values = {'$set': {'cookies': new_cookies}}

    profiles.cookies.update_one(query, new_values, upsert=True)

if __name__ == "__main__":
    pass