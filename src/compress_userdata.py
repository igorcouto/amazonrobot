import os
import re
import sys
import inflect
import zipfile
import argparse
import checksumdir
from time import sleep
from hurry.filesize import size
from common.utils import get_logger
from os.path import basename, abspath, join, dirname, exists, getsize


def get_hash(folder):
    logger = get_logger('get_hash')
    folder_path = abspath(folder)
    try:
        h = checksumdir.dirhash(folder_path)
        return h
    except IOError as message:
        logger.warning('IOError: %s to hash %s. Maybe in use.' % (message.strerror, basename(folder)))
     
def unzip(file, dest_folder):
    """Unzips a .zip file in a destination folder.
    
    Parameters
    ----------
    file : str
        The .zip file to unzip
    dest_folder : str
        The folder to store the unzipped filed.
    """
    filepath = abspath(file)
    _zip = zipfile.ZipFile(filepath)
    _zip.extractall(join(dest_folder))

    _zip.close()

def get_folders(root):
    """Gets all directories in a root.
    
    Parameters
    ----------
    root: str
        The root directory path.
    Return
    ------
    directories : list
        A list that contains all directories absolute paths in root.
    """
    folders = []
    for subfolder in os.listdir(root):
        subfolder_path = join(root, subfolder)
        if os.path.isdir(subfolder_path):
            folders.append(subfolder_path)
    
    return folders

def zip_folder(folder_path, output_path):
    """Zips the contents of an entire folder (with that folder included
    in the archive). Empty subfolders will be included in the archive
    as well.
    """
    logger = get_logger('zip_folder')
    parent_folder = dirname(folder_path)
    # Retrieve the paths of the folder contents.
    contents = os.walk(folder_path)
    try:
        zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
        for root, folders, files in contents:
            if 'PepperFlash' in folders:
                folders.remove('PepperFlash')
            if 'Cache' in folders:
                folders.remove('Cache')
            if 'Code Cache' in folders:
                folders.remove('Code Cache')
            if 'ShaderCache' in folders:
                folders.remove('ShaderCache')
            # Include all subfolders, including empty ones.
            for folder_name in folders:
                absolute_path = join(root, folder_name)
                relative_path = absolute_path.replace(parent_folder + '\\', '')
                zip_file.write(absolute_path, relative_path)
            for file_name in files:
                if re.search('exe$', file_name):
                    continue
                absolute_path = join(root, file_name)
                relative_path = absolute_path.replace(parent_folder + '\\', '')
                try:
                    zip_file.write(absolute_path, relative_path)
                except ValueError as message:
                    logger.warning('Could not be zipped: %s' % relative_path)

        filesize = size(getsize(output_path))
        logger.info("%s (%s) created successfully." % (basename(output_path),
                                                       filesize))
        zip_file.close()

        return True
    except IOError as message:
        logger.warning('IOError: %s to zip %s. Maybe in use.' % (message.strerror, basename(folder_path)))
        print(message)
    except OSError as message:
        logger.warning('OSError: %s to zip %s. Maybe is in use.' % (message.strerror, basename(folder_path)))
    except zipfile.BadZipfile as message:
        logger.warning('BadZipfile: Impossible to write in %s' % basename(output_path) )
    
    return False

def parse_arguments():
    """Parses the arguments passed by command line.
    
    Returns
    -------
    args : argparse.ArgumentParser
        The object with the user options
    """
    # Main parser
    parser = argparse.ArgumentParser(description='Compress user-data directories.',
                                     prog='python src\\compress_user_data.py',
                                     usage='%(prog)s [options]')
    parser.add_argument('-source',
                        dest='USERDATA_FOLDER',
                        default='data/temp',
                        help='Selects the folder which the users data are placed in.' +
                        ' By default, <PROJECT_FOLDER>\data\\temp is setted.')
    parser.add_argument('-destination',
                        dest='ZIPPED_USERDATA_FOLDER',
                        default='data/temp',
                        help='Selects the folder where the compressed users data will be placed in.' +
                        ' By default, <PROJECT_FOLDER>\data\\temp is setted.')
    parser.add_argument('-r',
                        dest='recursive',
                        action='store_true',
                        help='Keeps this process running indefinitely. A new folder search will' +
                             ' be done every X seconds.')
    parser.add_argument('-t',
                        dest='sleep_time',
                        type=int,
                        help='The amount of time to wait with the recursive flag. By dafault, '+
                             '60 seconds is setted.')
    # Instantiate the parser.
    args = parser.parse_args()
    
    # Argument verifications
    if bool(args.sleep_time) and not bool(args.recursive):
        parser.error('you need to use sleep time flag (-t) with recurvice flag (-r)')
    if bool(args.recursive):
        args.sleep_time = 60

    return args

def main():

    # Parse the arguments passe by the user
    options = parse_arguments()

    logger = get_logger('compress_userdata')
    pl = inflect.engine()

    # Source 
    USERDATA_FOLDER = abspath(options.USERDATA_FOLDER)
    if not os.path.exists(USERDATA_FOLDER):
        logger.error('Folder don\'t exist: %s' % USERDATA_FOLDER)
        sys.exit('Finishing')

    userdata_folders = get_folders(USERDATA_FOLDER)

    if not userdata_folders:
        logger.warning('There isn\'t user-data folders.')
    else:
        num = len(userdata_folders)
        logger.info('%s %s in %s.' % (num,
                                      pl.plural('folder', num),
                                      USERDATA_FOLDER))
    # Destination    
    ZIPPED_USERDATA_FOLDER = abspath(options.ZIPPED_USERDATA_FOLDER)
    if not os.path.exists(ZIPPED_USERDATA_FOLDER):
        os.mkdir(ZIPPED_USERDATA_FOLDER)

    hashes = {}
    while True:
        # Compresses userdata folders
        for folder in get_folders(USERDATA_FOLDER):
            # If the returns None, skip to next folder
            current_hash = get_hash(folder)
            if not current_hash:
                continue
            # Checks if the folder is new
            if folder in hashes.keys():
                # If didnt change, continue
                if current_hash == hashes.get(folder):
                    continue
                else:
                    logger.info('%s folder was changed.' % basename(folder))
                    hashes[folder] = current_hash
            else:
                logger.info('%s folder was founded!' % basename(folder))
                hashes[folder] = current_hash
            
            zip_filename = basename(folder) + '.zip'
            zip_filepath = abspath(join(ZIPPED_USERDATA_FOLDER, zip_filename))
            # Compress the folder
            zip_folder(folder, zip_filepath)
        
        # If recurvice flag is not activated. Go away
        if not options.recursive:
            break
        
        # Waits a little to repeat.
        sleep(options.sleep_time)

if __name__ == '__main__':
    main()