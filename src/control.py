import re
import argparse
from robots.controller import launch_robot


def parse_arguments():
    """Parses the arguments passed by command line.
    
    Returns
    -------
    args : argparse.ArgumentParser
        The object with the user options
    """
    # Common arguments
    common_parser = argparse.ArgumentParser(add_help=False)
    
    # Proxy options
    common_parser.add_argument('-proxy',
                               dest='proxy_service',
                               action='store',
                               choices=['ml', 'netnut'],
                               help='Select a proxy for use.')
    common_parser.add_argument('-puser',
                               dest='proxy_user',
                               action='store',
                               help='Set a proxy user for proxies services ' +
                                    'that requires authentication.')
    common_parser.add_argument('-ppwd',
                               dest='proxy_pwd',
                               action='store',
                               help='Set a proxy password for proxies ' +
                                    'services that requires authentication.')
    common_parser.add_argument('-pip',
                               dest='proxy_server_ip',
                               help='If a Microleaves proxy service is selected, ' +
                               'you can specify a proxy server IP to connect.'+
                               ' By default, a random IP will be used.')
    common_parser.add_argument('-pport',
                               dest='proxy_server_port',
                               help='If a Microleaves proxy service is selected, ' +
                                    'you can specify a proxy server port to connect.'+
                                    ' By default, a random port will be used.')    
    
    # Browser options
    common_parser.add_argument('-nwrtc',
                               dest='webRTC_protection',
                               action='store_false',
                               default=True,
                               help='Disable WebRTC protection. It avoids Internal ' +
                                    'IP leaks by WebRTC.')
    common_parser.add_argument('-ntzp',
                               dest='tz_protection',
                               action='store_false',
                               default=True,
                               help='Disable TimeZone protection. It avoids system ' +
                                    'time to be used to discover the real location of the robot.')
    common_parser.add_argument('-ndetach',
                               dest='detach',
                               action='store_false',
                               default=True,
                               help='Disable detached mode. If this flag is used, the browser ' +
                                    'ends when the process is completed. By default, the'+
                                    ' browser remains opened even when the process ends.')
    common_parser.add_argument('-nbe',
                               dest='buyer_env_check',
                               action='store_false',
                               default=True,
                               help='Disable buyer environment verification.')

    # Main parser
    parser = argparse.ArgumentParser(description='Script to start a robot.',
                                     prog='python src\\control.py',
                                     usage='%(prog)s <robot> [options]')
    subparsers = parser.add_subparsers(title='robots',
                                       help='Select a robot to execute.')
    subparsers.required = True
    subparsers.dest = 'robot'

    # SimpleRobot
    sim_name = 'robot'
    sim_msg = 'This robot only configures a instance and opens a page.'
    sim_parser = subparsers.add_parser(sim_name,
                                       description=sim_msg,
                                       parents=[common_parser],
                                       prog='python src\\control.py %s' % sim_name,
                                       usage='%(prog)s [options]',
                                       help=sim_msg)
    sim_parser.set_defaults(func=launch_robot)
    sim_parser.add_argument('-c',
                            dest='config_file',
                            default='settings/%s.ini' % sim_name,
                            help='The path of configuration INI file of the robot.'+
                                 ' By default, the INI is %s.ini.' % sim_name)
    sim_parser.add_argument('-cname',
                            dest='customer_name',
                            action='store',
                            help='The name of the customer to create the Amazon account.'+
                                 ' If not informed, the robot will keep this field blank.')
    sim_parser.add_argument('-cemail',
                            dest='customer_email',
                            action='store',
                            help='The email to create the Amazon account.')
    sim_parser.add_argument('-cpwd',
                            dest='customer_password',
                            action='store',
                            help='The password to create the Amazon account.')
    
    # Instantiate the parser.
    args = parser.parse_args()
    
    # Argument verifications
    if bool(args.proxy_user) ^ bool(args.proxy_pwd):
        parser.error('proxy user (-puser) and proxy password (-ppwd) must ' +
                     'be given together')
    
    if args.proxy_service == 'netnut':
        if not bool(args.proxy_user) and not bool(args.proxy_pwd):
            parser.error('Netnut proxy service requires authentication. ' +
                         'Proxy user (-puser) and proxy password (-ppwd) ' +
                         'must be provided')

    if args.robot == 'robot':
        if (not(bool(args.customer_name) and
            bool(args.customer_email) and 
            bool(args.customer_password))):
            parser.error('you need to set customer name (-cname), email ' +
                         '(-cemail) and password (-cpwd)')

    if (args.proxy_service != 'ml' and 
        (bool(args.proxy_server_ip) or
         bool(args.proxy_server_port))):
        parser.error('proxy server IP (-pip) and proxy server port ' +
                     '(-pport) can be used only with Microleaves proxy ' +
                     ' service.')

    return args

def main():
    """The initial method. Here, the parameters passed by the user and
    configurations files are converted in options to be used ahead.

    The next step will be call the initiator methods, to start a robot.
    """
    # Parse the arguments passe by the user
    options = parse_arguments()

    print('\n AmazonRobot command-line options:')
    for opt in vars(options):
        print(' - %s: %s' % (opt, getattr(options, opt)))
    
    print('')
    # Starts the procedures of robot initialization 
    options.func(options=options)

if __name__ ==  '__main__':
    main()