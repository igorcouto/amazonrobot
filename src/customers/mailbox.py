import pytz
import email
import imaplib
import logging
import tzlocal
import lxml.html
from time import sleep
from datetime import datetime as dt, timedelta as td


def get_first_text_block(email_message):
    """Search on email message payload the first text type.

    Parameters
    ----------
    email_message : email.message.Message
        An instance of email.message.Message returned by a fetch.

    Return
    ------
    payload : str
        The text payload itself.
    """
    maintype = email_message.get_content_maintype()
    # If the first payload is multipart, go inside it
    if maintype == 'multipart':
        for part in email_message.get_payload():
            # Continues until find a text payload
            if part.get_content_maintype() == 'text':
                return part.get_payload()

    # If is a text payload, only returns
    elif maintype == 'text':
        return email_message.get_payload()

def convert_email_to_dict(email_message):
    """Converts an email message in a dictionary.
    
    Parameters
    ----------
    email_message : email.message.Message
        An instance of email.message.Message returned by a fetch.
    
    Return
    ------
    email_dict : dict
        The email in a dictionary format
    """
    email_dict = {}
    email_dict['from'] = email_message['From']
    email_dict['to'] = email_message['To']
    email_dict['subject'] = email_message['Subject']
    # Date
    email_dt = dt.strptime(email_message['Date'], '%a, %d %b %Y %H:%M:%S %z')
    email_dict['date'] = email_dt
    # Content
    email_content = get_first_text_block(email_message)
    email_dict['content'] = email_content

    return email_dict

def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    return logger

def wait_for_verification_code(username, password, attempts=10, time_bw_attempts=5):
    """Waits for the verification code arrives in customer mailbox.

    Parameters
    ----------
    attempts : int
        The number of times that the robot will attempt.
    time_bw_attempts : int
        The time between each attempt.
    
    Return
    ------
    code : str
        The verification code itself
    """
    # Open an IMAP connection to wait for verification email
    mail = MailBox(username=username,
                    password=password,
                    server='imap.mail.yahoo.com')
    mail.login()

    while (range(attempts)):
        # Current datetime
        now_dt = pytz.utc.localize(dt.utcnow())

        sleep(time_bw_attempts) # zZzZz...

        # Get emails from mailbox. 
        verification_emails = mail.get_emails_by_sender('account-update@amazon.com')
        verification_emails.reverse()

        for e in verification_emails:
            # Before compare datetime, convert to local timezone
            now_dt = now_dt.astimezone(tzlocal.get_localzone())            
            email_dt = e['date'].astimezone(tzlocal.get_localzone())

            # Email datetime is less than 10 minutes
            if not now_dt - email_dt > td(minutes=10):
                date_format = '%H:%M:%S on %Y/%m/%d'
               
                # Convert content to HTML and parse using xpath to search the code.
                content_in_html = lxml.html.fromstring(e['content'])
                amz_code = content_in_html.xpath('//p[@class="otp"]/text()')[0]
                mail.logout()
                return amz_code
          
    mail.logout()

class MailBox():
    def __init__(self, username, password, server, port=993):
        self.logger = get_logger('MailBox')
        self.CONNECTION = imaplib.IMAP4_SSL(server, port)
        self.USERNAME = username
        self.PASSWORD = password

    def login(self):
        """Executes log in.
        """
        self.logger.info('Logging with %s.' % self.USERNAME)
        self.CONNECTION.login(self.USERNAME, self.PASSWORD)

    def get_emails_by_subject(self, subject, folder='Inbox'):
        """Get all emails with the same subject
        
        Parameters
        ----------
        subject : str
            A subject to select emails.
        folder : str
            Select a folder where the emails will be search.

        Return
        ------
        emails : list
            A list of dictionaries 
        """
        self.logger.info('Searching emails with subject "%s".' % subject)
        # Select the email folder
        self.CONNECTION.select(folder)
        # Store emails in a list of dicts
        emails = []
        # Make the search based on subject
        search_status, search_data = self.CONNECTION.uid('search', None, '(SUBJECT "%s")' % subject)
        if search_status == 'OK':
            for _id in search_data[0].split():
                # Fetch one by one
                fetch_status, fetch_data = self.CONNECTION.uid('fetch', _id, '(RFC822)')
                if fetch_status == 'OK':
                    email_message = email.message_from_bytes(fetch_data[0][1])
                    email_dict = convert_email_to_dict(email_message)
                    emails.append(email_dict)
        
        self.logger.info('%s emails was found.' % len(emails))
        return emails

    def get_emails_by_sender(self, sender, folder='Inbox'):
        """Get all emails with the same sender.
        
        Parameters
        ----------
        subject : str
            The sender to select emails.
        folder : str
            Select a folder where the emails will be search.

        Return
        ------
        emails : list
            A list of dictionaries 
        """
        self.logger.info('Searching emails with sender "%s".' % sender)
        # Select the email folder
        self.CONNECTION.select(folder)
        # Store emails in a list of dicts
        emails = []
        # Make the search based on subject
        search_status, search_data = self.CONNECTION.uid('search', None, '(FROM "%s")' % sender)
        if search_status == 'OK':
            for _id in search_data[0].split():
                # Fetch one by one
                fetch_status, fetch_data = self.CONNECTION.uid('fetch', _id, '(RFC822)')
                if fetch_status == 'OK':
                    email_message = email.message_from_bytes(fetch_data[0][1])
                    email_dict = convert_email_to_dict(email_message)
                    emails.append(email_dict)
        
        self.logger.info('%s emails was found.' % len(emails))
        return emails

    def logout(self):
        """Executes log out.
        """
        self.logger.info('Logging out.')
        self.CONNECTION.logout()
