from robots.initiator import prepare_environment
from atc_mechanism.forwarder import forward_by_external_link
from robots.shopping import Shopping
from customers.mailbox import wait_for_verification_code
from resources.profile_switcher import update_cookies
from common.utils import read_config


def robot_factory(options):
    """Here it will be placed all robot lifecycle. Browser creation,
    initialization, run robot actions, etc.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    """
    driver, personal_info, cookies = prepare_environment(options)
    while True:
        if driver:
            break

    if not options.atc_disabled:
        forward_by_external_link(driver, options)
    else:
        driver.get('https://amazon.com')

    robot = Shopping(driver=driver)

    if not robot.is_logged_in():
        robot.sign_in()
        robot.fill_login_form(username=personal_info['email'],
                            password=personal_info['password'])

        if robot.verification_needed():
            code = wait_for_verification_code(username=personal_info['email'],
                                            password=personal_info['password'])
            robot.enter_the_code(code)
    
    config_file = read_config(options.config_file)
    configurations = dict(vars(config_file)['_sections'])
    configurations = {k: dict(v) for k, v in configurations.items()}

    update_cookies(options=options,
                   configurations=configurations,
                   driver=driver,
                   email=personal_info['email'])

def start_robot(options):
    """Here it will be placed the preparation of the robots. Browser creation,
    initialization, run robot actions, etc.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    """
    
    from common.utils import read_config
    # Read the robot configuration file and convert in a dict
    config_file = read_config(options.config_file)
    configurations = dict(vars(config_file)['_sections'])
    configurations = {k: dict(v) for k, v in configurations.items()}

    # Check if exists a the profile. It not, create one.
    import sys
    from resources.utility import read_csv
    from random import choice
    from datetime import datetime as dt
    profiles = get_database(configurations)

    personal_info = profiles.personal_info.find_one({'email': options.customer_email})
    if personal_info and personal_info.get('amz_acc') == 'OK':
        print('\nSorry. Email is already in use.')
        print(personal_info)
        sys.exit('\nFinishing.')
    if not personal_info:
        print('\nCreating personal_info document for %s.' % options.customer_email)
        personal_info = {'name': options.customer_name,
                         'email': options.customer_email,
                         'password': options.customer_password,
                         'address_country': 'US',
                         'last_access': dt(2019, 1, 1),
                         'amz_acc': 'PENDING'}
        profiles.personal_info.insert_one(personal_info)
        
    settings = profiles.settings.find_one({'email': options.customer_email})
    if not settings:
        print('Creating settings document for %s.' % options.customer_email)
        settings_sample = read_csv('data/settings_samples.csv', delimiter=',')
        settings = choice(settings_sample)
        settings['email'] = options.customer_email
        profiles.settings.insert_one(settings)
    
    cookies = profiles.cookies.find_one({'email': options.customer_email})

    options.username = options.customer_email
    options.password = options.customer_password
    options.last_access_days = 0

    driver, personal_info, cookies = prepare_environment(options)

    while True:
        if driver:
            break

    if not options.atc_disabled:
        forward_by_external_link(driver, options)

    driver.execute_script('window.open("https://mail.yahoo.com");')

    update_cookies(options=options,
                   configurations=configurations,
                   driver=driver,
                   email=personal_info['email'])
    """
    if personal_info.get('amz_acc') == 'PENDING':
        query = {'email': options.customer_email}
        new_values = {'$set': {'amz_acc': 'OK'}}
        profiles.personal_info.update_one(query, new_values, upsert=True)
    """

def get_database(configurations):
    from pymongo.errors import InvalidURI
    from resources.mongodb_atlas import MongoDB_Atlas
    import sys
    try:
        mongo = MongoDB_Atlas(cluster=configurations['MONGODB']['mdb_cluster'],
                              username=configurations['MONGODB']['mdb_username'],
                              password=configurations['MONGODB']['mdb_password'])
        db = mongo.get_database(configurations['MONGODB']['mdb_database'])
    except InvalidURI:
        sys.exit('\nAn error occurred to access MongoDB Atlas. Please check if the '+
                 'parameters are configured on file.')
    return db