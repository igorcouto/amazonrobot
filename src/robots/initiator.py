import pprint
from selenium import webdriver
from common.utils import read_config
from resources.profile_switcher import get_profile
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from resources.proxy_switcher import set_proxy, proxy_authentication
from resources.supervisors import is_a_buyer_environment, load_cookies


def print_all_options_configurations(options, config, personal_info, cookies, settings,
                                     proxy_service):
    """Prints some variables on prompt.
    """

    print('\nCONFIGURATIONS')
    pprint.pprint(config)

    print('\nPROFILE')
    pprint.pprint(personal_info)
    if settings:
        pprint.pprint(settings)
    else:
        print("{'Settings': 'No'}")

    if cookies.get('cookies'):
        print("{'Cookies': 'Yes'}")
    else:
        print("{'Cookies': 'No'}")
    
    print('\nPROXY SERVICE :', options.proxy_service)
    pprint.pprint(proxy_service)

def prepare_environment(options):
    """Prepares the environment to the robot with the options and configurations passed by user.
    Before to start the robot:
        - Select a profile

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.

    Returns
    driver : WebDriver
        A Chrome driver object that will be controlled by a robot. Or return None if any verification fails.
    """
    
    # Read the robot configuration file and convert in a dict
    config_file = read_config(options.config_file)
    configurations = dict(vars(config_file)['_sections'])
    configurations = {k: dict(v) for k, v in configurations.items()}

    # Retrieve a profile from profile Switcher
    personal_info, cookies, settings, chrome_opt = get_profile(options=options,
                                                               configurations=configurations)
    # Set proxy configuration
    chrome_opt, proxy_service = set_proxy(options=options,
                                          configurations=configurations,
                                          chrome_options=chrome_opt)

    # Print the variable on prompt
    print_all_options_configurations(options,
                                     configurations,
                                     personal_info,
                                     cookies,
                                     settings, 
                                     proxy_service)
    
    # Setting disable_images from .ini configuration file
    if configurations['DRIVER']['disable_images']:
        prefs = {'profile.managed_default_content_settings.images':2}
        chrome_opt.add_experimental_option("prefs", prefs)

    # If WebRTC protection is enabled, so add WebRTC Control extension
    if options.webRTC_protection:
        chrome_opt.add_extension(configurations['EXTENSIONS']['webrtc_control'])

    # If TimeZone protection is enabled, so add SpoofTimeZone extension
    if options.tz_protection:
        chrome_opt.add_extension(configurations['EXTENSIONS']['spoof_timezone'])

    if options.detach:
        chrome_opt.add_experimental_option("detach", True)

    chrome_opt.add_extension(configurations['EXTENSIONS']['cookies_txt'])
    chrome_opt.add_argument('user-data-dir=data/profiles/%s' % personal_info['email'])
    
    # Creating the browser instance
    print('\nBROWSER INITIALIZATION:')
    driver = webdriver.Chrome(executable_path=configurations['DRIVER']['executable_path'],
                              options=chrome_opt)

    
    # Some proxies services requires authetication.
    proxy_authentication(driver=driver, options=options)

    # If buyer environment is enabled, check the browser/connection.
    if options.buyer_env_check:
        print('\nBUYER ENVIRONMENT VERIFICATION:')
        if not is_a_buyer_environment(driver=driver):
            return None

    # Loading the cookies on browser instance.
    #if cookies.get('cookies'):
    #    load_cookies(driver, cookies.get('cookies'))

    return driver, personal_info, cookies

if __name__ == "__main__":
    pass
