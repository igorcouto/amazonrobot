import os
import sys
import zipfile
from selenium import webdriver
from pymongo import MongoClient
from random import choice, randint
from datetime import datetime as dt
from compress_userdata import unzip
from pymongo.errors import InvalidURI
from os.path import abspath, exists, join, basename
from common.utils import read_config, get_logger, read_csv
from selenium.common.exceptions import InvalidArgumentException, WebDriverException


def get_client(username, password, cluster):
    """Gets personal_info and setting of customer from clould.

    Parameters
    ----------
    username : str
        The username to connect on MongoDB - Atlas
    password : str
        The password to connect on MongoDB - Atlas
    cluster : str
        The cluster name on MongoDB - Atlas
    """
    logger = get_logger('get_client')
    conn_string = (
        'mongodb+srv://%s:%s@%s.mongodb.net/test?retryWrites=true&w=majority' % (
            username,
            password,
            cluster
        )
    )
    try:
        client = MongoClient(conn_string)
    except InvalidURI:
        logger.error('An error occurred to access MongoDB Atlas. Please check if' +
                     ' the parameters are configured.')
        sys.exit('Finishing.')
    return client

def retrieve_customer_info(db, name, email, password):
    """Retrives customer information from MongoDB instance
    """
    # Creates a logger
    logger = get_logger('retrieve_customer_info')

    personal_info = db.personal_info.find_one({'email': email})
    settings = db.settings.find_one({'email': email})
    
    # Check if documents exists on cloud
    if not personal_info:
        logger.warning('There is not a personal_info for %s. ' % email +
                       'A new document will be created' )
        personal_info = {'name': name,
                         'email': email,
                         'password': password,
                         'address_country': 'US',
                         'last_access': dt(2019, 1, 1),
                         'amz_acc': 'PENDING'}
        # Uploading document to cloud
        db.personal_info.insert_one(personal_info)

    else:
        logger.info('There is a personal_info for %s. Loaded!' % email)

    if not settings:
        logger.warning('There is not a setting for %s. ' % email +
                       ' A new document will be created.' )
        settings_sample = read_csv('data/settings_samples.csv')
        settings = choice(settings_sample)
        settings['email'] = email
        # Uploading document to cloud
        db.settings.insert_one(settings)

    else:
        logger.info('There is a settings for %s. Loaded!' % email)
    
    return personal_info, settings

def create_driver(options):
    """Creates a driver based on command line options.
    
    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    """
    # Creates a logger
    logger = get_logger('create_driver')
    # Loads the robot configuration .ini file
    config = read_config(options.config_file)
    
    # Creating a client to connect NoSQL database
    client = get_client(username=config.get('MONGODB', 'mdb_username'),
                        password=config.get('MONGODB', 'mdb_password'),
                        cluster=config.get('MONGODB', 'mdb_cluster'))
    
    # NoSQL database
    profiles =  client.get_database(config.get('MONGODB', 'mdb_database'))

    # Retriving customer information from cloud
    personal_info, settings = retrieve_customer_info(profiles,
                                                     options.customer_name,
                                                     options.customer_email,
                                                     options.customer_password)

    # Retrieves profiles data from local folder
    # Loads the folder to stored temporary files
    temp_folder = abspath(config.get('FOLDERS', 'temp'))
    if not exists(temp_folder):
        os.mkdir(temp_folder)

    # Loads where the profiles are stored in
    zipped_userdata_folder = config.get('FOLDERS', 'zipped_userdata')
    zipped_userdata = abspath(join(zipped_userdata_folder, options.customer_email)) + '.zip'

    temp_userdata = abspath(join(temp_folder, options.customer_email))

    if not exists(zipped_userdata):
        logger.warning('There is not a zipped user-data in %s.' % config.get('FOLDERS',
                                                                             'zipped_userdata'))
        if exists(temp_userdata):
            logger.info('There is a temp user-data in %s.' % config.get('FOLDERS',
                                                                        'temp'))
    else:
        logger.info('Extracting user-data directory on %s.' % config.get('FOLDERS',
                                                                            'temp'))
        unzip(zipped_userdata, temp_folder)

    ### CHROME OPTIONS
    # Creating browser options to update based on command line options and 
    # .ini configurations
    chrome_options = webdriver.ChromeOptions()

    # Settings document
    chrome_options.add_argument('--lang=%s' % settings.get('languages'))
    chrome_options.add_experimental_option(
        'prefs',
        {'intl.accept_languages': settings.get('languages')}
    )
    chrome_options.add_argument('window-size=%s,%s' % (settings.get('w_resolution'),
                                                       settings.get('h_resolution')))

    # User-data directory
    chrome_options.add_argument('user-data-dir=%s/%s' % (temp_folder,
                                                         personal_info['email']))

    # If WebRTC protection is enabled, so add WebRTC Control extension
    if options.webRTC_protection:
        chrome_options.add_extension(config.get('EXTENSIONS', 'webrtc_control'))

    # If TimeZone protection is enabled, so add SpoofTimeZone extension
    if options.tz_protection:
        chrome_options.add_extension(config.get('EXTENSIONS', 'spoof_timezone'))
    
    # If detach mode is enabled
    if options.detach:
        chrome_options.add_experimental_option("detach", True)

    # Microleaves proxy service. No authentication needed.
    if options.proxy_service == 'ml':
        if options.proxy_server_ip:
            server = options.proxy_server_ip
        else:
            server = '62.210.169.169'

        if options.proxy_server_port:
            port = options.proxy_server_port
        else:
            port = 3266 + randint(0, 7)

        proxy_host = '%s:%s' % (server, port)
        # Adding on ChromeOption object
        chrome_options.add_argument('--proxy-server=%s' % proxy_host)
        
    # NetNut proxy service. Authentication needed.
    if options.proxy_service == 'netnut':
        proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
        # Adding on ChromeOption object
        chrome_options.add_argument('--proxy-server=%s' % proxy_host)
        chrome_options.add_extension(config.get('EXTENSIONS', 'proxy_autoauth'))

    # Creating browser
    logger.info('Creating a browser instance.')
    
    chrome_options.add_argument("--log-level=3")

    # Block images extension
    chrome_options.add_extension(config.get('EXTENSIONS', 'block_img'))

    # IP Supervisor extension
    chrome_options.add_extension(config.get('EXTENSIONS', 'ip_supervisor'))

    try:
        driver = webdriver.Chrome(executable_path=config.get('DRIVER', 'executable_path'),
                                  options=chrome_options)
    except InvalidArgumentException as ia:
        logger.error(ia.msg)
        sys.exit('Finishing.')
    except WebDriverException as wde:
        logger.error(wde.msg)
        sys.exit('Finishing.')

    return driver

def launch_robot(options):
    """Launches the robot based on command-line options.

    Parameters
    ----------
    options : a namespace object
        The parameters passed by command line.
    """
    # Creating a logger.
    logger = get_logger('launch_robot')

    # Creating a browser instance
    driver = create_driver(options)

    # Go to check2ip page.
    if options.buyer_env_check:
        driver.get('http://check2ip.com')

    # Open a new page
    if options.buyer_env_check:
        driver.execute_script('window.open("https://mail.yahoo.com");')
    else: 
        driver.get('https://mail.yahoo.com')

    print('')
    logger.info('Detaching the browser from the terminal. Do not forget to zip the ' +
                'user-data later.')
