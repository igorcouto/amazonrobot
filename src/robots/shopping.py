from selenium.webdriver.common.by import By
from common.utils import random_sleep, get_logger
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class Shopping():
    def __init__(self, driver, wait_time=10):
        self.DRIVER = driver
        self.logger = get_logger('Shopping')
        self.WAIT = WebDriverWait(driver, wait_time)

    def sign_in(self):
        """Only goes to the login page.
        """
        self.logger.info('Going to login page.')
        try:
            self.WAIT.until(lambda d: d.find_element_by_id('nav-signin-tooltip')).click()
            random_sleep(min=1, max=2) # zZzZz...
            return True
        except TimeoutException:
            pass
        # If did not find "Sign in" button, click on "Accounts & Lists".
        nav_accountlist = self.WAIT.until(lambda d: d.find_element_by_id('nav-link-accountList'))
        
        try:
            self.ACTION.move_to_element(nav_accountlist).perform()
            self.WAIT.until(lambda d: d.find_element_by_id('nav-flyout-ya-signin')).click()
            random_sleep(min=1, max=2) # zZzZz...
            return True
        except TimeoutException:
            pass
        
        WebDriverWait(self.DRIVER, self.WAIT_TIME).until(
            ec.visibility_of_element_located((By.LINK_TEXT, 'Your Lists'))
        ).click()
        random_sleep(min=1, max=2) # zZzZz...

    def fill_login_form(self, username, password):
        """Fills the login form with customer information.
        """
        self.logger.info('Filling the login form.')
        self.WAIT.until(lambda d: d.find_element_by_id('ap_email')).send_keys(username)
        self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()
        random_sleep(min=1, max=2) # zZzZz...
        self.WAIT.until(lambda d: d.find_element_by_id('ap_password')).send_keys(password)
        random_sleep(min=1, max=2) # zZzZz...
        self.WAIT.until(lambda d: d.find_element_by_id('signInSubmit')).click()
        random_sleep(min=1, max=2) # zZzZz...

    def verification_needed(self):
        """Verifies if Amazon shows a verification page. And clicks on "Continue"
        or "Send Code" button.

        Return
        ------
        success : bool
            If True, Amazon took the robot to verification page. Otherwise,
            go shopping.
        """
        # Verification needed?
        try:
            verify_form_xpath = '//form[@name="claimspicker" and @action="verify"]'
            self.WAIT.until(lambda d: d.find_element_by_xpath(verify_form_xpath))
            self.logger.warning('Verification needed.')
            random_sleep(min=1, max=2) # zZzZz...
            self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()
            return True
        except TimeoutException:
            return False

    def enter_the_code(self, code):
        """Enter the verification code.
        
        Parameters
        ----------
        code : str
            The code sent to customer email.
        """
        self.logger.info('Entering the code %s on verification form.' % code)
        self.DRIVER.find_element_by_xpath('//input[@name="code"]').send_keys(code)
        random_sleep(min=1, max=2) # zZzZz...
        self.DRIVER.find_element_by_xpath('//input[@type="submit"]').click()

    def is_logged_in(self):
        try:
            login_msg = self.WAIT.until(lambda d: d.find_element_by_xpath('//*[@id="nav-link-accountList"]/span/text()'))
            if login_msg != 'Hello, Sign in':
                print('Já está loigado')
                return True
        except TimeoutException:
            pass
            
            return False
        