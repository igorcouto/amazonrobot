# Amazon Robot

A [Selenium](https://www.seleniumhq.org/) project to promote products on Amazon.

### Prepare the code
  - Install [GIT](https://git-scm.com/downloads)
  - Install [Python 3.7.4](https://www.python.org/downloads/release/python-374/). 
    - Do not forget to add python on your PATH.
    - Other versions of Python 3 can be used.
  - Download the code:
    - Open a terminal, select a working directory and type:
        ```
        $ git clone https://gitlab.com/upfreelas/amazonrobot
        ```
    - A folder called amazonbot will be created with all project files. Enter on the folder:
        ```
        $ cd amazonrobot
        ```
    - Install the project dependencies:
        ```
        $ pip install -r requirements.txt
        ```
  - Install [Google Chrome](https://www.google.com/chrome).
  - Download [ChromeDriver](http://chromedriver.chromium.org/downloads) executables according to your Google Chrome version.
    - On the project folder, creates a folder called *chromedriver*. Put the chromedriver executable there.
  - The code is ready!

  ### Settings
  - In *settings* folder, edit the .ini file corresponds to the robot you wanna use. For instance: shoppingrobot.ini > shoppingrobot.py.
  - You need edit the varibles in .ini file. The most important is the proxy user and password.
    - user = <USERNAME>
    - pwd = <PASSWORD>
  - Online:
    - If you use resources from Google Cloud Platform, please put your credential JSON file in *credentials* folder. And assure that your resources (spreadsheets) on cloud are available and configured as well.
  - Offline:
    - In other hand, if you prefer use resources from you local computer, keep them available on *data* folder. When you run a robot, add the offline flag.
      ```
      $ python run_shoppingrobot.py --offline
      ```
    - Note that you need specific the location of files on .ini file.
    - If you do not have this files on your computer, download them from Google Cloud Platform:
      - [customers](https://docs.google.com/spreadsheets/d/1XaczJzDKzb_EhusiWftKBeYpuqYIwserr68Wi9_Nk5s/edit#gid=0)
      - [products](https://docs.google.com/spreadsheets/d/1gLYkWE8F7GPKMyTR597HYkGft2SiKDYUbS7y_GiZf9I/edit#gid=0)
      - [products_tracking](https://docs.google.com/spreadsheets/d/1HqYMTqROx_PfovUb5w4Oy7vCEHOP--epevcIxt3JbDA/edit#gid=0)
    - Go in File > Download > Comma-separeted values (.csv, current sheet). Put them on *data* folder.
  
  You have the robot ready to use!

  ### Usage
  - On command line prompt, go to root project folder and type:
      ```
      $ python run_shoppingrobot.py
      ```